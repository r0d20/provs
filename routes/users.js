var express = require('express');
var router = express.Router();
var sqlHelper = require('./db').userOps;
var bcrypt = require('bcrypt');
const saltRounds = 10;

router.get('/registroLogin', function(req,res,next){
  res.render('admin.pug',{title:'Agregar usuario'});
});

router.post('/registro', function(req,res,next){
  
  req.checkBody('usuario').notEmpty().isLength(4,20);
  req.checkBody('password').matches(/^[a-zA-Z0-9|._\-!"#$%&]{8,20}$/, 'g');
  req.checkBody('repassword').equals(req.body.password);
  
  req.getValidationResult().then((result)=>{
    if(!result.isEmpty())result.throw();

    let usuario = req.body.usuario;
    const contraseña = req.body.password;
    
    bcrypt.hash(contraseña, saltRounds, function(err, hash_contraseña) {
      sqlHelper.creaUsuario(usuario, hash_contraseña);
    });
    let msjCompletado = 'Usuario agregado!';
    res.render('admin.pug', {title:'Agregar usuario', completado:msjCompletado});
  }).catch((result)=>{
      res.status(400).render('admin.pug', {title:'Agregar usuario', error:'Error al crear usuario'});
  });
  
});

router.post('/login', function(req, res, next) {
  //validar login
  //passport
    // let hash = sqlHelper.hashUsuario(req.body.usuario);  
    // bcrypt.compare(req.body.password, hash, function(err, res) 
    // res == true
    //crear sesion
    
    //res == false
    //redirect pagina inicio
    
  // });
  //res.render('provs.pug');
  res.redirect('/proveedores/');
});

router.get('/logout', function(req, res, next) {
  //destruir sesion
  res.redirect('/');
});

module.exports = router;
