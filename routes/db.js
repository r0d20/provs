const mysql = require('mysql');

const pool = mysql.createPool(
    {
        host:'localhost',
        user: 'root',
        password: '',
        database: 'proveedores'
    }
);

const setupDB = () => {
    var init = mysql.createConnection({host:'localhost', user: 'root', password: ''});
    init.query('CREATE DATABASE IF NOT EXISTS proveedores', (err, result)=>{
        if(err){    
             if(err.code === 'ECONNREFUSED')
                 err.message += ' No hay conexion a la base de datos'
             //console.log(err.message);
            //return err;
            throw err;
        }
    });
    init.end();

    pool.query('CREATE TABLE IF NOT EXISTS proveedor (id INT AUTO_INCREMENT PRIMARY KEY, nombre VARCHAR(255), rfc VARCHAR(13), calle VARCHAR(255), numExt VARCHAR(6), colonia VARCHAR(255), contableMX VARCHAR(10), anticiposMX VARCHAR(10), contableDLS VARCHAR(10), anticiposDLS VARCHAR(10))', 
    (err,result)=>{
        if(err){    
            return err;
            //throw err;
        }
    }); 
}
var auth = function(msj){
    this.msj = 'clase auth: '+ msj;
    this.verMsj = ()=>{
        return this.msj;
    }
    this.verOtroMsj = ()=>{
        return 'clase auth otro msj'
    }
}
var userOps = {
    creaUsuario : (usuario,contraseña)=>{
        let sql = 'insert into usuario (nombre, contraseña) values (?,?)';
        pool.query(sql, [usuario, contraseña], (err, result)=>{
            if(err) throw err;
        });
    },
    hashUsuario : (usuario)=>{
        let sql = 'select contraseña from usuario where nombre = ?';
        pool.query(sql,[usuario], sql, (err, result)=>{
            if(err) throw err;
        });
    }
};
var dbOps = {
    getConexion : (callback)=>{
        pool.getConnection((err, conexion)=>{
            if(conexion !== undefined)
                conexion.release();
            if(err){    
                console.log("desconectado mysql "+ err.code);
                return callback(err);
                //throw err;
            }
            console.log("conectado mysql");
            callback(null);
        });
    },
    
    getProveedor : (idProv, callback)=>{
        // let sql = 'select * from proveedor where id = ?';
        let sql = 'select id, nombre, rfc, calle, numExt, colonia, contableMX as cuentaContableMXN, anticiposMX as cuentaAnticiposMXN, contableDLS as cuentaContableDLS, anticiposDLS as cuentaAnticiposDLS from proveedor where id = ?';
        let json = '';
        pool.query(sql, [idProv], (err, result)=>{
            //if(err) throw err;
            // console.log("get proveedor id db");
            // console.log(err);
            if(err)
                return callback(err, null);

            else if(result.length < 1)  
                return callback(null, result);

            json = JSON.stringify(result);
            callback(null, json);
            // console.log(result);
        });
        console.log('get proveedores')
    },
    getProveedores : (callback)=>{
        let sql = 'select * from proveedor';
        let json = '';
        pool.query(sql, (err, result)=>{
            //if(err) throw err;
            if(err)
                return callback(err, null);

            json = JSON.stringify(result);
            callback(null, json);
            // console.log(result);
        });
        console.log('get proveedores')
    },
    updateProveedor : (idProv, req)=>{
        let sql = 'update proveedor set nombre=?, rfc=?, calle=?, numExt=?, colonia=?, contableMX=?, anticiposMX=?, contableDLS=?, anticiposDLS=? where id = ?';
        console.log(req);
        pool.query(sql, [req.nomcia, req.rfc, req.calle, req.numExt, req.colonia, req.cuentaContableMXN, req.cuentaAnticiposMXN, req.cuentaContableDLS,req.cuentaAnticiposDLS, idProv], (err, result)=>{
            if(err) throw err;
        });
        console.log('update proveedor')
    },
    postProveedor : (req)=>{
        let sql = 'insert into proveedor (nombre, rfc, calle, numExt, colonia, contableMX, anticiposMX, contableDLS, anticiposDLS) values (?,?,?,?,?,?,?,?,?)';
        // console.log(req);
        pool.query(sql, [req.nomcia, req.rfc, req.calle, req.numExt, req.colonia, req.cuentaContableMXN, req.cuentaAnticiposMXN, req.cuentaContableDLS,req.cuentaAnticiposDLS], (err, result)=>{
            if(err) throw err;
        });
        console.log('post proveedor')
    }
};

//exports.pool = pool;
module.exports = {setupDB, dbOps, userOps, auth};
// module.exports = {setupDB, dbOps, auth};
