const express = require('express');
const router = express.Router();
const sqlHelper = require('./db').dbOps;

router.get('/alta', function(req, res, next) {
    res.render('prov_alta', { title:'Alta de Proveedor'});
});

router.get('/:id([0-9]+)', function(req, res, next) {
    
    let resultJson = (err, result) => {
        if(err || result.length < 1){
            console.log("get proveedor id error");
            return next(err);
        }
        console.log("get proveedor id ok"+result);
        res.render('prov_alta', { title:'Editar Proveedor', prov:result, idProv:req.params.id});
        //res.redirect('/');
        
    };
    console.log("get proveedor id");
    sqlHelper.getProveedor(req.params.id, resultJson);
    // console.log(req.params.id);
    // res.render('prov_alta', { title:'Editar Proveedor'});
});

router.get('/', function(req, res, next) {
    let resultJson = (err, result) => {
        if(err){
            return next(err);
        }
        res.render('prov_lista.pug', {title: 'Proveedores', provs:result});
    };
    sqlHelper.getProveedores(resultJson);
});

router.post('/:id([0-9]+)', function(req,res, next){
    req.checkBody('nomcia', 'Nombre invalido').notEmpty().isLength(4,255);
    req.checkBody('rfc', 'RFC invalido').isLength(13,13);
    req.checkBody('calle', 'Calle invalida, 4 caracteres mínimo').notEmpty().isLength(4,255);
    req.checkBody('numExt','numExt requerido, 6 caracteres máximo, ej. 2314').notEmpty().isLength(1,6);
    req.checkBody('colonia', 'Colonia invalida, 4 caracteres mínimo').notEmpty().isLength(4,255);
    req.getValidationResult()
    .then(function(result) {
        if(!result.isEmpty()) result.throw();
        // console.log(req.headers);
        // console.log(req.body);
        if(req.params.id != req.body.id)
            result.throw();
        
        sqlHelper.updateProveedor(req.params.id, req.body);

        let jsonArr=  '['+ JSON.stringify(req.body) +']';
        jsonArr = jsonArr.replace("nomcia","nombre");
        console.log(jsonArr);

        let msjCompletado = 'Edición completada!';
        res.render('prov_alta', { title:'Editar Proveedor', id:req.params.id, prov:jsonArr,  completado: msjCompletado});

    }).catch(function(result) {
        let msjError = 'Error al editar proveedor en la base de datos';
        console.log('errores');
        if(result)
            console.log(result);
        res.status(400).render('prov_alta', { title:'Editar Proveedor', error: msjError});
    })
});

router.post('/', function(req, res, next){
    req.checkBody('nomcia', 'Nombre invalido').notEmpty().isLength(4,255);
    req.checkBody('rfc', 'RFC invalido').isLength(13,13);
    req.checkBody('calle', 'Calle invalida, 4 caracteres mínimo').notEmpty().isLength(4,255);
    req.checkBody('numExt','numExt requerido, 6 caracteres máximo, ej. 2314').notEmpty().isLength(1,6);
    req.checkBody('colonia', 'Colonia invalida, 4 caracteres mínimo').notEmpty().isLength(4,255);
    req.getValidationResult()
    .then(function(result) {
        if(!result.isEmpty()) result.throw();

        sqlHelper.postProveedor(req.body);
        let msjCompletado = 'Registro completado!'
        res.render('prov_alta', { title:'Alta de Proveedor', completado: msjCompletado});

    }).catch(function(result) {
        let msjError = 'Error al registrar proveedor en la base de datos';
        console.log('errores');
        console.log(result.mapped());
        res.status(400).render('prov_alta', { title:'Alta de Proveedor', error: msjError});
    })
    
});

module.exports = router;